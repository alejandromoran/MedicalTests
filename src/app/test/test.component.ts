import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from './../data.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {
  public test: any;
  public score: number;
  public totalQuestions: number;
  public question: any;
  public questionGroup: any;
  public currentPage: number;
  public maxSize: number;
  public finished: boolean;

  constructor(private route: ActivatedRoute, private dataService: DataService, private router: Router) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => {
      return false;
    };
    this.initData();
  }

  ngOnInit() {
    this.route.params.subscribe((params: { name: string }) => {
      this.dataService.getTest(params.name).subscribe((test: any) => {
        this.initData();
        let count = 1;
        test.questionsGroups.forEach((questionGroup: any) => {
          questionGroup.questions.forEach((question: any) => {
            question.id = count;
            count = count + 1;
          });
        });
        this.totalQuestions = count - 1;
        this.test = test;
        this.question = this.getQuestionById(this.currentPage);
        this.questionGroup = this.getQuestionGroupByQuestionId(this.currentPage);
        const event = {
          page: this.currentPage,
          itemsPerPage: 1
        };
        this.pageChanged(event);
      });
    });
  }

  private initData() {
    this.maxSize = 5;
    this.score = 0;
    this.totalQuestions = 0;
    this.currentPage = 1;
    this.finished = false;
    this.question = null;
    this.questionGroup = null;
  }

  public getQuestionById(id: number) {
    let questionFound = [];
    this.test.questionsGroups.forEach((questionGroup: any) => {
      questionGroup.questions.forEach((question: any) => {
        if (question.id === id) {
          questionFound = question;
          return;
        }
      });
    });
    return questionFound;
  }

  public getQuestionGroupByQuestionId(id: number) {
    let questionGroupFound;
    this.test.questionsGroups.forEach((questionGroup: any) => {
      if (questionGroup.questions.find((question: any) => question.id === id)) {
        questionGroupFound = questionGroup;
      }
    });
    return questionGroupFound;
  }

  public answerSelected(checkedIndex: number) {
    this.question.checkedIndex = checkedIndex;
    this.updateScore();
    const event = {
      page: this.currentPage + 1,
      itemsPerPage: 1
    };

    const unansweredQuestionsIndexes = this.getUnansweredQuestions();
    if (unansweredQuestionsIndexes.length > 0) {
      if (unansweredQuestionsIndexes.indexOf(this.currentPage + 1) === -1) {
        event.page = unansweredQuestionsIndexes[0];
      }
      this.pageChanged(event);
    } else {
      this.finished = true;
    }
  }

  private updateScore() {
    let score = 0;
    this.test.questionsGroups.forEach((questionGroup: any) => {
      questionGroup.questions.forEach((question: any) => {
        if (question.checkedIndex >= 0) {
          score = score + question.answers[question.checkedIndex].value;
        }
      });
    });
    this.score = score;
  }

  public pageChanged(event: { page: number; itemsPerPage: number }) {
    this.currentPage = event.page;
    this.question = this.getQuestionById(event.page);
    this.questionGroup = this.getQuestionGroupByQuestionId(event.page);
  }

  private getUnansweredQuestions() {
    const unansweredQuestionsIndexes = [];
    this.test.questionsGroups.forEach((questionGroup: any) => {
      questionGroup.questions.forEach((question: any) => {
        if (!(question.checkedIndex >= 0)) {
          unansweredQuestionsIndexes.push(question.id);
        }
      });
    });
    return unansweredQuestionsIndexes;
  }
}
