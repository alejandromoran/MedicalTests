export class Question {
    title: string;
    description: string;
    answers: Object[];
}