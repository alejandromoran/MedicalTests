import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ContactComponent } from './contact.component';
import { CONTACT_ROUTES } from './contact.routes';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(CONTACT_ROUTES),
    ReactiveFormsModule
  ],
  declarations: [
    ContactComponent
  ]
})
export class ContactModule { }
