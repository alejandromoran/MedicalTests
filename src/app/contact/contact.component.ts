import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { NotificationsService } from 'angular2-notifications';
import * as emailjs from 'emailjs-com';
import { environment } from 'environments/environment';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  public contactForm: FormGroup;

  constructor(private fb: FormBuilder, private notificationsService: NotificationsService) {}

  ngOnInit() {
    this.createContactForm();
  }

  public onSubmit() {
    emailjs.init(environment.contact.userId);
    emailjs.send(environment.contact.serviceId, environment.contact.templateId, this.contactForm.value).then(
      (response) => {
        this.notificationsService.success('Email enviado!', 'Gracias, le enviaremos una respuesta pronto');
      },
      (error) => {
        this.notificationsService.success('Lo sentimos!', 'El email no ha podido ser enviado, inténtelo de nuevo más tarde');
      }
    );
  }

  private createContactForm() {
    this.contactForm = this.fb.group({
      name: '',
      email: '',
      message: ''
    });
  }

}
