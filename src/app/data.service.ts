import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class DataService {

  private testsData: any;
  private baseTestsPath: string;

  constructor(private httpClient: HttpClient) {
    this.baseTestsPath = './assets/tests';
    this.testsData = {
      GDS: {
        file: 'gds.json',
        title: 'Escala de depresión geriátrica (YESAVAGE): Cuestionario corto',
      },

      tam: {
        file: 'tam.json',
        title: 'Test de Alteración de la Memoria'
      }
    };
  }

  /*

        mecLobo: {
        file: 'mecLobo.json',
        title: 'MEC Lobo'
      },
      minimentalState: {
        file: 'minimentalState.json',
        title: 'Minimental State'
      },

      */
  public getTest(testName: string) {
    return this.httpClient.get(`${this.baseTestsPath}/${this.testsData[testName].file}`);
  }

  public getTestsSummary() {
    const testsKeys = Object.keys(this.testsData);
    return testsKeys.map((testKey: string) => {
      return {
        name: testKey,
        title: this.testsData[testKey].title
      };
    });
  }

}
