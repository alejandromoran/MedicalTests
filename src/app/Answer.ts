export class Answer {
    title: string;
    value: boolean;
    score: Number;
}