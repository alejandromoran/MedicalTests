import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from './../data.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  tests: any[];
  public show: boolean;

  constructor(private dataService: DataService, private router: Router) {
    this.show = false;
  }

  ngOnInit() {
    this.tests = this.dataService.getTestsSummary();
  }

  public switchShow() {
    this.show = !this.show;
  }

  public navigateToTest(path: string, testName: string) {
    this.router.navigate([path, testName]);
  }

}
