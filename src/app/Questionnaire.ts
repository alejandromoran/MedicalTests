import { Question } from './Question';

export class Questionnaire {
    name: string;
    title: string;
    description: string;
    questions: Object[];
}