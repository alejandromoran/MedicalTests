import { Routes } from "@angular/router";
import { HomeComponent } from "./home/home.component";
import { TestComponent } from "./test/test.component";

export const APP_ROUTES: Routes = [
    {
        path: '',
        component: HomeComponent
    },
    {
        path: 'contact',
        loadChildren: './contact/contact.module#ContactModule'
    },
    {
        path: 'test/:name',
        component: TestComponent
    }
  ];
