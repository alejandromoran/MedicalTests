![](https://travis-ci.org/AlejandroMoran/MedicalTests.svg?branch=master)

# MedicalTests
A variety of medical tests

Live demo: [Medical Tests](https://alejandromoran.github.io/MedicalTests)

List of covered tests
* T@m (Test de Alteración de la Memoria) - https://www.hipocampo.org/TAM.pdf
* Geriatric Depression Scale - https://consultgeri.org/try-this/general-assessment/issue-4-spanish.pdf
* MEC Lobo - file:///Users/alejandromoran/Downloads/S037747321170075X_S300_es%20(1).pdf
* Minimental State - http://www.ics.gencat.cat/3clics/guies/30/img/minimentaldef.MMSE.pdf
* Short Physical Performance Battery - https://research.ndorms.ox.ac.uk/prove/documents/assessors/outcomeMeasures/SPPB_Protocol.pdf
* Velocidad de la marcha (m/s)
* Criterios de fragilidad (Linda Fried Frailty Criteria)
* Mini Nutritional Assessment

## Contact
Software Engineer: Alejandro Morán - sac@alejandromoran.com
Geriatric Doctor: Carmen Ramón Otero
